# Google Plus Image Downloader
## Before
- NodeJS with NPM ([Read More](https://www.npmjs.com/get-npm))
- Google Plus API Key ([Read More](https://developers.google.com/+/web/api/rest/))
- Git ([Read More](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git))

## Preparation
First of all, Set your google api key:
```bash
export googlePlusApiKey="YOUR_API_KEY"
```

Open terminal in path that you want to store this project:
```bash
cd /path/to/
```

Clone it:
```bash
git clone https://gitlab.com/lenchan139/google-plus-image-downloader.git
```

Install typescript compiler(tsc command):
```bash
sudo npm i -g typescript
```

Install denpendency:
```bash
npm i
```

Run it:
```bash
npm start
```

## Common Problems
### How to get user id?
Open Google Plus on desktop version. Go to any post of the user. right click the user's icon, copy the url may like this:
`https://plus.google.com/108478884981163771065`
the number like `108478884981163771065` is user id.

### I run it with `npm start` but it say tsc command not found.
run command `sudo npm i -g typescipt` first. DON'T FORGET SUDO IF Unix/Linux.

### Why NodeJS not Python?
I hate Python, that's all. OK?

### Hi, your code look so suck, like your english.
Contribute this or shut up.

## License
This Project is publish under MIT License.
```
The MIT License

Copyright (c) 2010-2018 Len Chan https://tto.moe

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
```
